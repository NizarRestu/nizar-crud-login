package com.crud.react.service_impl;

import com.crud.react.exception.InternalErrorException;
import com.crud.react.exception.NotFoundException;
import com.crud.react.model.Products;
import com.crud.react.repository.ProductsRepository;
import com.crud.react.service.ProductsService;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ProductServiceImpl implements ProductsService {

    private static final String DOWNLOAD_URL = "https://firebasestorage.googleapis.com/v0/b/upload-image-example-a0910.appspot.com/o/%s?alt=media";
    @Autowired
    ProductsRepository productsRepository;

    @Override
    public Products addProducts(Products products , MultipartFile multipartFile) {
        String img = imageConverter(multipartFile);
        Products products1 = new Products(products.getNama(),products.getDeskripsi(),img, products.getHarga());
        return productsRepository.save(products1);
    }

    private String uploadFile(File file, String fileName) throws IOException {
        BlobId blobId = BlobId.of("upload-image-example-a0910.appspot.com", fileName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("media").build();
        Credentials credentials = GoogleCredentials.fromStream(new FileInputStream("./src/main/resources/serviceAccountKey.json"));
        Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
        storage.create(blobInfo, Files.readAllBytes(file.toPath()));
        return String.format(DOWNLOAD_URL, URLEncoder.encode(fileName, StandardCharsets.UTF_8));
    }

    private String getExtentions(String fileName) {
        return fileName.split("\\.")[0];
    }

    private String imageConverter(MultipartFile multipartFile) {
        try {
            String fileName = getExtentions(multipartFile.getOriginalFilename());
            File file = convertToFile(multipartFile, fileName);
            var RESPONSE_URL = uploadFile(file, fileName);
            file.delete();
            return RESPONSE_URL;
        } catch (Exception e) {
            e.getStackTrace();
            throw new InternalErrorException("Error upload file");
        }
    }

    private File convertToFile(MultipartFile multipartFile, String fileName) throws IOException {
        File file = new File(fileName);
        try (FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(multipartFile.getBytes());
            fos.close();
        }
        return file;
    }
    @Override
    public List<Products> getAll(String query) {
        return productsRepository.searchProducts(query);
    }

    @Override
    public Products getProduct(Integer id) {
        return productsRepository.findById(id).orElseThrow(() -> new NotFoundException("Tidak Ada"));
    }

    @Override
    public Products editProduct(Integer id, Products products) {
        Products update = productsRepository.findById(id).orElseThrow(() -> new NotFoundException(("Tidak Ada")));
        update.setNama(products.getNama());
        update.setDeskripsi(products.getDeskripsi());
        update.setHarga(products.getHarga());
        return productsRepository.save(update);
    }

    @Override
    public Map<String, Boolean> deleteProduct(Integer id) {
        try {
            productsRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }
}
