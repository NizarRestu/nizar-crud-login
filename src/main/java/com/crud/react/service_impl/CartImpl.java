package com.crud.react.service_impl;

import com.crud.react.DTO.CartDTO;
import com.crud.react.exception.NotFoundException;
import com.crud.react.model.Cart;
import com.crud.react.model.Products;
import com.crud.react.repository.CartRepository;
import com.crud.react.repository.ProductsRepository;
import com.crud.react.repository.UsersRepository;
import com.crud.react.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CartImpl implements CartService {
    @Autowired
    CartRepository cartRepository;
    @Autowired
    UsersRepository usersRepository;
    @Autowired
    ProductsRepository productsRepository;


    @Override
    public Cart addCart(CartDTO cart) {
        Cart create = new Cart();
        Products products = productsRepository.findById(cart.getProductId()).orElseThrow(() -> new NotFoundException("Product tidak ditemukan"));
        create.setJumlah(cart.getJumlah());
        create.setTotalHarga(products.getHarga() * cart.getJumlah());
        create.setUser(usersRepository.findById(cart.getUserId()).orElseThrow(() -> new NotFoundException("User tidak ditemukan")));
        create.setProducts(products);

       return cartRepository.save(create);
    }

    @Override
    public Cart updateCart(int id, CartDTO cart) {
        Cart data = cartRepository.findById(id).orElseThrow(()-> new NotFoundException("Id tidak ditemukan"));
        data.setJumlah(cart.getJumlah());
        data.setUser(usersRepository.findById(cart.getUserId()).orElseThrow(() -> new NotFoundException("User tidak ditemukan")));
        data.setProducts(productsRepository.findById(cart.getProductId()).orElseThrow(() -> new NotFoundException("Product tidak ditemukan")));
        return cartRepository.save(data);

    }

    @Override
    public List<Cart> findAll(int userId) {
        return cartRepository.findUser(userId);
    }

    @Override
    public Map<String,Boolean> checkout(int userId) {
       List<Cart> carts = cartRepository.findUser(userId);
       cartRepository.deleteAll(carts);
        Map<String, Boolean> res = new HashMap<>();
        res.put("Checkout", Boolean.TRUE);
        return res;

    }

    @Override
    public Map<String, Boolean> deleteCart(int id) {
        try {
            cartRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }
}
