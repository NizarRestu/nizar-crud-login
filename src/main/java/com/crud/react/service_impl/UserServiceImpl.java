package com.crud.react.service_impl;

import com.crud.react.DTO.LoginDto;
import com.crud.react.enumated.UserEnum;
import com.crud.react.exception.InternalErrorException;
import com.crud.react.exception.NotFoundException;
import com.crud.react.jwt.JwtProvider;
import com.crud.react.model.Users;
import com.crud.react.repository.UsersRepository;
import com.crud.react.service.UserService;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {
    private static final String DOWNLOAD_URL = "https://firebasestorage.googleapis.com/v0/b/upload-image-example-a0910.appspot.com/o/%s?alt=media";
    @Autowired
    UsersRepository usersRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    UserDetailsService userDetailsService;
    @Autowired
    private JwtProvider jwtProvider;

    @Override
    public Users addUsers(Users users, MultipartFile multipartFile) {
        String email = users.getEmail();
        String profil = imageConverter(multipartFile);
        Users users1 = new Users(users.getEmail(), users.getPassword(), users.getNama(), users.getAlamat(), profil, users.getTelepon(), users.getRole());
        if (users.getRole().name().equals("ADMIN"))
            users.setRole(UserEnum.ADMIN);
        else users.setRole(UserEnum.USER);

        var validasi = usersRepository.findByEmail(email);
        if (validasi.isPresent()) {
            throw new InternalErrorException("Maaf Email sudah digunakan");
        }
        users1.setPassword(passwordEncoder.encode(users.getPassword()));
        return usersRepository.save(users1);
    }

    private String uploadFile(File file, String fileName) throws IOException {
        BlobId blobId = BlobId.of("upload-image-example-a0910.appspot.com", fileName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("media").build();
        Credentials credentials = GoogleCredentials.fromStream(new FileInputStream("./src/main/resources/serviceAccountKey.json"));
        Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
        storage.create(blobInfo, Files.readAllBytes(file.toPath()));
        return String.format(DOWNLOAD_URL, URLEncoder.encode(fileName, StandardCharsets.UTF_8));
    }

    private String getExtentions(String fileName) {
        return fileName.split("\\.")[0];
    }

    private String imageConverter(MultipartFile multipartFile) {
        try {
            String fileName = getExtentions(multipartFile.getOriginalFilename());
            File file = convertToFile(multipartFile, fileName);
            var RESPONSE_URL = uploadFile(file, fileName);
            file.delete();
            return RESPONSE_URL;
        } catch (Exception e) {
            e.getStackTrace();
            throw new InternalErrorException("Error upload file");
        }
    }

    private File convertToFile(MultipartFile multipartFile, String fileName) throws IOException {
        File file = new File(fileName);
        try (FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(multipartFile.getBytes());
            fos.close();
        }
        return file;
    }

    private String authories(String email, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        } catch (BadCredentialsException e) {
            throw new InternalErrorException("Email Or Password Not Found");
        }
        UserDetails userDetails = userDetailsService.loadUserByUsername(email);
        return jwtProvider.generateToken(userDetails);
    }

    @Override
    public Map<String, Object> login(LoginDto loginDto) {
        String token = authories(loginDto.getEmail(), loginDto.getPassword());
        Users users = usersRepository.findByEmail(loginDto.getEmail()).get();
        Map<String, Object> response = new HashMap<>();
        response.put("Token", token);
        response.put("expired", "15 menit");
        response.put("user", users);
        return response;
    }

    @Override
    public List<Users> getAll() {
        return usersRepository.findAll();
    }

    @Override
    public Users getUsers(Integer id) {
        return usersRepository.findById(id).orElseThrow(() -> new NotFoundException("Tidak Ada"));
    }

    @Transactional
    @Override
    public Users editUsers(Integer id, Users users) {
        Users update = usersRepository.findById(id).orElseThrow(() -> new NotFoundException("Tidak Ada"));
        var validasi = usersRepository.findByEmail(users.getEmail());
        if (validasi.isPresent()) {
            throw new InternalErrorException("Maaf email sudah ada");
        }
        update.setEmail(users.getEmail());
        update.setPassword(users.getPassword());
        return update;

    }

    @Override
    public Map<String, Boolean> deleteUsers(Integer id) {
        try {
            usersRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }
}
