package com.crud.react.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "temporary_token")
public class TemporaryToken {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String token;

    @Column(name = "expired_date")
    private Date expiredDate;
    @Column(name = "user_id")
    private int userId;

    public TemporaryToken() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public String







    toString() {
        return "TemporaryToken{" +
                "id=" + id +
                ", token='" + token + '\'' +
                ", expiredDate=" + expiredDate +
                ", userId=" + userId +
                '}';
    }
}
