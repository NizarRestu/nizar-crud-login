package com.crud.react.controller;

import com.crud.react.DTO.ProductDto;
import com.crud.react.model.Products;
import com.crud.react.response.CommonResponse;
import com.crud.react.response.ResponseHelper;
import com.crud.react.service.ProductsService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {
    @Autowired
    ProductsService productsService;
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping(consumes = "multipart/form-data")
    public CommonResponse<Products> addProduct( ProductDto productDto, @RequestPart("file") MultipartFile multipartFile ) {
        return ResponseHelper.ok(productsService.addProducts(modelMapper.map(productDto , Products.class) , multipartFile));
    }
    @GetMapping
    public CommonResponse<List<Products>> searchProducts(@RequestParam(name = "query", required = false) String query) {
        return ResponseHelper.ok(productsService.getAll(query ==null ? "" : query));
    }
    @GetMapping("/{id}")
    public CommonResponse<Products> getById(@PathVariable("id") Integer id) {
        return ResponseHelper.ok(productsService.getProduct(id));
    }
    @PutMapping("/{id}")
    public CommonResponse<Products> editProduct(@PathVariable("id") Integer id, @RequestBody
    ProductDto productDto) {
        return ResponseHelper.ok(productsService.editProduct(id, modelMapper.map(productDto , Products.class)));
    }
    @DeleteMapping("/{id}")
    public CommonResponse<?> deleteProduct(@PathVariable("id") Integer id){
        return ResponseHelper.ok(productsService.deleteProduct(id));
    }
}
