package com.crud.react.controller;

import com.crud.react.DTO.CartDTO;
import com.crud.react.model.Cart;
import com.crud.react.response.CommonResponse;
import com.crud.react.response.ResponseHelper;
import com.crud.react.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/cart")
public class CartController {
    @Autowired
    private CartService cartService;

    @PostMapping
    public CommonResponse<Cart> addCart(@RequestBody CartDTO cart) {
        return ResponseHelper.ok(cartService.addCart(cart));
    }
    @GetMapping
    public CommonResponse<List<Cart>> findAll(@RequestParam(name = "user_id") int userId){
        return ResponseHelper.ok(cartService.findAll(userId));
    }
    @PutMapping("/{id}")
    public CommonResponse<Cart> update(@PathVariable("id") int id, @RequestBody CartDTO cart) {
      return ResponseHelper.ok(cartService.updateCart(id, cart));
    }
    @DeleteMapping("/{id}")
    public CommonResponse<?>  delete(@PathVariable("id") int id) {
        return ResponseHelper.ok(cartService.deleteCart(id));
    }
    @DeleteMapping("/checkout")
    public CommonResponse<?>  checkout(@RequestParam(name = "user_id") int userId) {
        return ResponseHelper.ok(cartService.checkout(userId));
    }
}
