package com.crud.react.repository;

import com.crud.react.model.TemporaryToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import java.util.Optional;

@Repository
public interface TemporaryTokenRepository extends JpaRepository<TemporaryToken, Integer> {
    Optional<TemporaryToken> findByToken(String token);
    Optional<TemporaryToken> findByUserId(int userId);
}
