package com.crud.react.repository;

import com.crud.react.model.Products;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductsRepository extends JpaRepository<Products,Integer> {
    @Query(value = "SELECT p.* FROM products p WHERE " +
            "p.nama LIKE CONCAT('%',:query, '%')", nativeQuery = true)
    List<Products> searchProducts(String query);
}
