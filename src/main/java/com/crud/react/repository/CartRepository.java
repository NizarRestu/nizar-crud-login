package com.crud.react.repository;

import com.crud.react.model.Cart;
import com.crud.react.model.Products;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CartRepository  extends JpaRepository<Cart , Integer> {
    @Query(value = "SELECT * FROM cart  WHERE user_id = ?1", nativeQuery = true)
    List<Cart> findUser(int userId);
    //List<Cart> findAllById(User userId);
}
