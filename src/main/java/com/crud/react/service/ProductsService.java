package com.crud.react.service;

import com.crud.react.model.Products;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface ProductsService {
    Products addProducts(Products products,MultipartFile multipartFile);

    List<Products> getAll(String query);

    Products getProduct(Integer id);

    Products editProduct(Integer id, Products products);

    Map<String, Boolean> deleteProduct(Integer id);
}
