package com.crud.react.service;

import com.crud.react.DTO.CartDTO;
import com.crud.react.model.Cart;

import java.util.List;
import java.util.Map;

public interface CartService {
    Cart addCart( CartDTO cart);

    Cart updateCart(int id, CartDTO cart);

    List<Cart> findAll(int userId);

    Map<String,Boolean> checkout(int userId);

    Map<String,Boolean> deleteCart(int id);
}
