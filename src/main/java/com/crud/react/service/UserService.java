package com.crud.react.service;


import com.crud.react.DTO.LoginDto;
import com.crud.react.model.Users;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface UserService {
    Users addUsers(Users users, MultipartFile multipartFile);

    Map<String ,Object> login(LoginDto loginDto);

    List<Users> getAll();

    Users getUsers(Integer id);

    Users editUsers(Integer id , Users users);

    Map<String, Boolean> deleteUsers(Integer id);
}
